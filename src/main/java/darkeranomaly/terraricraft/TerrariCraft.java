package darkeranomaly.terraricraft;

import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import darkeranomaly.terraricraft.blocks.*;

@Mod(modid=TerrariCraft.MODID, name=TerrariCraft.MODNAME, version=TerrariCraft.VERSION)
public class TerrariCraft {
	@Instance(TerrariCraft.MODID)
	public static TerrariCraft instance;
	public static final String MODNAME = "TerrariCraft";
	public static final String MODID = "terraricraft";
	public static final String VERSION = "0.0.0.0";
	
	public static final Block DemoniteOre = new DemoniteOre();
	public static final Block CobaltOre = new CobaltOre();
	@EventHandler
	public void PreInit(FMLPreInitializationEvent event){
		GameRegistry.registerBlock(DemoniteOre, "demonite_ore");
		GameRegistry.registerBlock(CobaltOre, "cobalt_ore");
	}
	@EventHandler
	public void Init(FMLInitializationEvent event){
		if(event.getSide().isClient()){
			registerItemRenders();
		}
	}
	@SideOnly(Side.CLIENT)
	public void registerItemRenders(){
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher()
		.register(net.minecraft.item.Item.getItemFromBlock(DemoniteOre), 0,
				new ModelResourceLocation(TerrariCraft.MODID + ":demonite_ore", "inventory"));
		Minecraft.getMinecraft().getRenderItem().getItemModelMesher()
		.register(net.minecraft.item.Item.getItemFromBlock(CobaltOre), 0,
				new ModelResourceLocation(TerrariCraft.MODID + ":cobalt_ore", "inventory"));
	}
}
