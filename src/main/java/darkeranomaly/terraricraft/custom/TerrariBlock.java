package darkeranomaly.terraricraft.custom;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class TerrariBlock extends Block {
	protected boolean isReflective = false;

	protected TerrariBlock(Material materialIn) {
		super(materialIn);
	}
	
	public boolean getIsReflective(){
		return this.isReflective;
	}
	
	public void setIsReflective(boolean isreflective){
		this.isReflective = isreflective;
	}

}
