package darkeranomaly.terraricraft.custom;

import java.util.Set;

import com.google.common.collect.Sets;

import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.item.ItemTool;

public class ItemTerrariPickaxe extends ItemTool {
	private static final Set EFFECTIVE_ON = Sets.newHashSet(new Block[] {Blocks.activator_rail, Blocks.coal_ore, Blocks.cobblestone, Blocks.detector_rail, Blocks.diamond_block, Blocks.diamond_ore, Blocks.double_stone_slab, Blocks.golden_rail, Blocks.gold_block, Blocks.gold_ore, Blocks.ice, Blocks.iron_block, Blocks.iron_ore, Blocks.lapis_block, Blocks.lapis_ore, Blocks.lit_redstone_ore, Blocks.mossy_cobblestone, Blocks.netherrack, Blocks.packed_ice, Blocks.rail, Blocks.redstone_ore, Blocks.sandstone, Blocks.red_sandstone, Blocks.stone, Blocks.stone_slab, Blocks.clay, Blocks.dirt, Blocks.farmland, Blocks.grass, Blocks.gravel, Blocks.mycelium, Blocks.sand, Blocks.snow, Blocks.snow_layer, Blocks.soul_sand});
    
	protected ItemTerrariPickaxe(ToolMaterial material) {
		super(1.5F, material, EFFECTIVE_ON);
	}
	//TODO: Wipe out vanilla's pickaxes, replace them with terraria pickaxes. Remove the vanilla recipes, and hide them from creative and nei.
	//Planned Pickaxe Tiers:
	//0: Copper, Cactus, Tin, Iron, Lead, Silver
	//1: Tungsten, Bone
	//2: Gold, Candy Cane, Platinum
	//3: Nightmare, Deathbringer
	//4: Molten, Reaver Shark
	//5: Cobalt, Palladium
	//6: Mythril, Orichalcum, Adamantite, Titanium
	//7: Chlorophyte, Spectre, Pickaxe Axe, Shroomite Digging Claw
	//8: Picksaw
}
