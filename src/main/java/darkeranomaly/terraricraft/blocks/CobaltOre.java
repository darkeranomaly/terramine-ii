package darkeranomaly.terraricraft.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import darkeranomaly.terraricraft.custom.TerrariBlock;

public class CobaltOre extends TerrariBlock {

	public CobaltOre() {
		super(Material.rock);
		setLightLevel(0F);
		setResistance(4.0F);
		setHardness(3.1F);
		setUnlocalizedName("cobalt_ore");
		setCreativeTab(CreativeTabs.tabBlock);
	}

}