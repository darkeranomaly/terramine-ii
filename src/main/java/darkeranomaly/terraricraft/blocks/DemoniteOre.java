package darkeranomaly.terraricraft.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import darkeranomaly.terraricraft.custom.TerrariBlock;

public class DemoniteOre extends TerrariBlock {

	public DemoniteOre() {
		super(Material.rock);
		setLightLevel(1.0F);
		setResistance(4.0F);
		setHardness(4.8F);
		setUnlocalizedName("demonite_ore");
		setCreativeTab(CreativeTabs.tabBlock);
	}

}
